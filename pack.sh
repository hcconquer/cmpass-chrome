#!/bin/sh

zip -r -9 cmpass-chrome.zip _locales/ background.html image/ manifest.json \
    options.html popup.html script/ style/
