(function(window) {

console.log('background start');

// var manifest = chrome.runtime.getManifest();

function showOptionsPage(tab) {
	var manifest = chrome.runtime.getManifest();
	var url = chrome.runtime.getURL(manifest.options_page);
	if (tab) {
		chrome.tabs.update(tab.id, {
			"url": url
		});
	} else {
		chrome.tabs.create(
			{
				"url": url
			}, 
			function(ntab) {
			}
		);
	}
}

function checkShowOptionsPage() {
	var manifest = chrome.runtime.getManifest();
	chrome.storage.sync.get(['version'], function(item) {
		var chg = 0; // not change
		if (item['version']) {
			if (manifest.version > item['version']) {
				// console.debug('update');
				chg = 1; // update
			} else if (manifest.version < item['version']) {
				chg = -1;
			}
		} else {
			// console.debug('install');
			chg = 2; // full new install
			saveOptions(null); // use default
		}
		console.debug("oldver: %s, newver: %s, chg: %d", item['version'], manifest.version, chg);
		if (chg != 0) {
			var url = chrome.runtime.getURL(manifest.options_page);
			chrome.tabs.create(
				{
					"url": url
				}, 
				function(tab) {
				}
			);
			chrome.storage.sync.set(
				{
					'version': manifest.version
				}, 
				function() {
				}
			);
		}
	});
}

function loginByBuJump(tab) {
	if (!tab) {
		return;
	}
	chrome.storage.local.set(
		{
			'auto_login_tabid': tab.id
		}, 
		function() {
		}
	);
	// http://caiyun.feixin.10086.cn
	// http://mail.10086.cn/
	chrome.tabs.update(tab.id, {
		"url": "http://mail.10086.cn/"
	});
}

function loginByBuSubmit(tab) {
	console.debug('loginByBuSubmit');
	chrome.tabs.sendRequest(tab.id, 
		{ 
			"action": "login" 
		}, 
		function(response) {
		}
	);
}

function loginByBuToPass(tab) {
	console.debug('loginByBuToPass');
	getPassAuth(function(auth) {
		var url = getSsoJumpUrl(auth, 3/*5*/); // from 139 mail, 3
		chrome.tabs.update(tab.id, {
			"url": url
		}, function(tab) {
			chrome.storage.local.remove(['auto_login_tabid']);
		});
	});
}

function loginAuto(tab) {
	chrome.storage.sync.get(null, function(item) {
		if (item['account'] && item['passwd']) {
			loginByBuJump(tab);
		} else {
			showOptionsPage(tab);
		}
	});
}

/*
 * used for set popup, title, menu e.g.
 */
function setCtxt(param) {
	console.debug('set ctxt, param: %s', JSON.stringify(param));
	if ((param) && (!$.isEmptyObject(param))) {
		var popup = '';
		if (param.authed) {
			popup = 'popup.html';
		}
		chrome.browserAction.setPopup({
			"popup": popup
		});
		
		var title = '';
		if ((param.authid) && (param.authid > 0)) {
			title = chrome.i18n.getMessage('title_user', [ param.authid ]);
		} else {
			var manifest = chrome.runtime.getManifest();
			title = chrome.i18n.getMessage('title_version', [ manifest.version ]);
		}
		chrome.browserAction.setTitle({
			"title": title
		});
	} else {
		checkPassAuth({}, function(data, status) {
			if (data && data['data'] && data.data['authid']) {
				setCtxt({ "authed": true, "authid": data.data['authid'] });
			} else {
				setCtxt({ "authed": false });
			}
		});
	}
}

/*
 * check auth and jump to bu page in special tab
 */
function checkAndJumpBuss(tab, bu) {
	console.debug('checkAndJumpBuss');
	checkPassAuth({}, function(data, status) {
		if (data && data['data'] && data.data['authid']) {
            setCtxt({ "authed": true, "authid": data.data['authid'] });
			var burl = getBuSsoUrl({}, bu);
			chrome.tabs.update(tab.id, {
				"url": burl
			}, function(tab) {
			});
		} else {
            setCtxt({ "authed": false });
        }
	});
}

/*
 * check auth and open new tab
 */
function checkAndOpenBuss(bu, callback) {
	console.debug('checkAndOpenBuss');
	checkPassAuth({}, function(data, status) {
		if (data && data['data'] && data.data['authid']) {
            setCtxt({ "authed": true, "authid": data.data['authid'] });
			var burl = getBuSsoUrl({}, bu);
			chrome.tabs.create({
				"url": burl
			}, function(tab) {
				callback(bu, tab);
			});
		} else {
            setCtxt({ "authed": false });
			var burl = getMainUrlByBu(bu);
			chrome.tabs.create({
				"url": burl
			}, function(tab) {
				callback(bu, tab);
			});
        }
	});
}

/*
 * focus or open bu tab
 * most time form popup menu
 */
function focusOrOpenBuTab(bu, callback) {
	var murl = getMatchUrlByBu(bu);
	chrome.tabs.query({
		"url": murl
	}, function(tabs) {
		if (tabs.length > 0) {
			var btab = tabs[0];
			chrome.tabs.update(btab.id, {
				"highlighted": true
			}, function(umctab) {
			});
		} else {
			checkAndOpenBuss(bu, function(bu, tab) { 
			});
		}
	});
}

function showMainPage(tab) {
	checkPassAuth({}, function(data, status) {
		var url = null, login = false;
		if (data && data['data'] && data.data['authid']) {
			setCtxt({ "authed": true, "authid": data.data['authid'] });
			url = "https://www.cmpassport.com/umc/";
		} else {
			setCtxt({ "authed": false });
			url = 'https://www.cmpassport.com/';
			login = true;
		}
		if (tab) {
			chrome.tabs.update(tab.id, {
				"url": url
			});
		} else {
			chrome.tabs.create({
				"url": url
			}, function(tab) {
				if (login) {
					loginAuto(tab);
				}
			});
		}
	});
}

/*
 * check is the any cmcc's bu can auto jump to main page from index/login
 */
function pageOpenListener(tab) {
	/* when open index, try auto jump */
	chrome.storage.sync.get(['auto_login'], function(item) {
		if (item['auto_login']) {
			chrome.storage.local.get(['auto_login_tabid'], function(item) {
				if (item['auto_login_tabid'] != tab.id) {
					if (tab.url == 'https://www.cmpassport.com/') {
						checkAndJumpBuss(tab, 0);
					} else if (tab.url == 'http://mail.10086.cn/') { 
						checkAndJumpBuss(tab, 3);
					} else if (tab.url == 'http://weibo.10086.cn/') {
						checkAndJumpBuss(tab, 4);
					} else if (tab.url == 'https://caiyun.feixin.10086.cn/') {
						/*
						 * caiyun's home page and login page is same
						 */
						checkCaiyunAuth(null, function(data) {
							if ((data) && (data['passid'] > 0)) {
								// do nothing
							} else {
								checkAndJumpBuss(tab, 5);
							}
						});
					} else if ((tab.url == 'http://pim.10086.cn/')
							|| (tab.url == 'http://pim.10086.cn/login.php')) {
						checkAndJumpBuss(tab, 6);
					}	
				}
			});
		}
	});
}

/*
 * when page is all loaded include css and images
 */
function pageReadyListener(tab) {
	// console.debug("page ready, tab: %s", JSON.stringify(tab));
	/* when page open but not load the dom
	 * you can't submit
	 * 
	 * if user set auto_login option, this is no used,
	 * but if not set, it will take effect
	 */
	chrome.storage.local.get(['auto_login_tabid'], function(item) {
		if (item['auto_login_tabid'] == tab.id) {
			loginByBuSubmit(tab);
		}
	});
}

function cookieChangedListener(info) {
	// console.debug("%s", JSON.stringify(info));
	var cookie = info.cookie;
	/*
	 * because when bu's page loaded, need some time to load sso plugin
	 * which will save cookie and can be jump to cmpassport.com
	 * 
	 * there are some important cookie:
	 * 1.Umc_PluginSSo_Sid, when set can sso jump
	 * 2.umckey, when set can get passid
	 */
	if (cookie.domain == '.cmpassport.com') {
		// console.debug("%s", JSON.stringify(info));
		if (cookie.name == 'Umc_PluginSSo_Sid') {
			console.debug('cookie, removed: %s, name: %s', info.removed, cookie.name);
			if (info.removed) {
				// setCtxt({ "authed": false });
			} else {
				/*
				 * sometime bu will keep session for long time
				 */
				// console.debug('save cookie %s', cookie.name);
				// console.debug(cookie);
				// setCtxt({}); // because not has passid in cookie, so get from request again
				chrome.storage.local.get(['auto_login_tabid'], function(item) {
					if (item['auto_login_tabid']) {
						chrome.tabs.get(item['auto_login_tabid'], function(tab) {
							if (tab) {
								loginByBuToPass(tab);
							} else { // handle exception, login tab has been closed
								chrome.storage.local.remove(['auto_login_tabid']);
							}
						});
					}
				});
			}
		} else if (cookie.name == 'agentid') { // cmpass reset login session's cookie
			/*
			if (info.removed) {
            } else {
				setCtxt({ "authed": false });
            }
			*/
        } else if (cookie.name == 'umckey') {
			if (info.removed) {
			} else {
				setCtxt({});
			}
		}
	}
}

function tabCreatedListener(tab) {
}

function tabUpdatedListener(tabId, event, tab) {
	// console.debug("%s %s %s", tabId, JSON.stringify(event), JSON.stringify(tab));
	if (event.status) {
		if (event.status == 'loading') {
			pageOpenListener(tab);
		} else if (event.status == 'complete') {
			pageReadyListener(tab);
		}
	}
}

function tabRemovedListener(tabId, info) {
}

function windowCreatedListener(window) {
	setCtxt({}); // when extension reload, need to check and set
	$('title').text(chrome.i18n.getMessage('title'));
}

function windowRemovedListener(winId) {
}

function popupClickedListener(tab) {
	chrome.tabs.query({
		"url": "*://*.cmpassport.com/*"
	}, function(tabs) {
		if (tabs.length > 0) { // cmpass tab opened
			var umctab = tabs[0];
			chrome.tabs.query({
				"active": true
			}, function(tabs) {
				var curtab = tabs[0];
				if (curtab.id == umctab.id) {
					showMainPage(curtab);
				} else {
					chrome.tabs.update(umctab.id, {
						"highlighted": true
					}, function(umctab) {
					});
				}
			});
		} else { // tab not open
			showMainPage(null);
		}
	});
}

/*
 * get request from page or popup
 */
function extensionRequestListener(request, sender, sendResponse) {
	console.debug(JSON.stringify(request));
	var resp = { 
		"action": request.action,
		"result": 0
	};
	if (request.action == 'focus_or_open_bu') {
		focusOrOpenBuTab(request.bu, function() {
		});
		sendResponse(resp);
	}
}

function onInstalledListener(info) {
	// extension install or update
	if ((info.reason != 'install') && (info.reason != 'update')) {
		return;
	}
	checkShowOptionsPage();
}

chrome.cookies.onChanged.addListener(cookieChangedListener);

chrome.tabs.onCreated.addListener(tabCreatedListener);
chrome.tabs.onUpdated.addListener(tabUpdatedListener);
chrome.tabs.onRemoved.addListener(tabRemovedListener);

chrome.windows.onCreated.addListener(windowCreatedListener);
chrome.windows.onRemoved.addListener(windowRemovedListener);

chrome.browserAction.onClicked.addListener(popupClickedListener);

chrome.extension.onRequest.addListener(extensionRequestListener);

chrome.runtime.onInstalled.addListener(onInstalledListener);

console.debug(JSON.stringify(chrome.runtime.getManifest()));

})(window);
