$(function() {

console.log('cmpass content start');

var handles = { 
	'getbu': function() { return -1; },
	'peer': function() { return false; },
	'input': function() { return false; },
	'login': function() { return false; },
	'rmad': function() { return false; },
	'fixs': function() { return false; }
};

function requestListener(request, sender, sendResponse) {
	var resp = { 
		"action": request.action,
		"result": 0
	};
	if (request.action == 'fill_acc_pwd') {
		handles['input']({
			"account": request.account,
			"passwd": request.passwd
		});
		sendResponse(resp);
	} else if (request.action == 'login') {
		handles['input']({
			"account": request.account,
			"passwd": request.passwd
		});
		handles['login']();
		sendResponse(resp);
	}
}

/*
 * register page handles
 */
(function(window) {
	var url = window.location.href;
	if (url.match('.*://.*cmpassport\.com/.*')) {
		handles['getbu'] = function(input) {
			return 0;
		}
		handles['input'] = function(input) {
			if (input.account) {
				$('#uid').val(input.account);
			}
			if (input.passwd) {
				$('#pwd').val(input.passwd);
			}
		}
		handles['login'] = function() {
			$('#btnlogin').click();
		}
		handles['rmad'] = function() {
			$('#adArea').hide();
			$('.adlink').hide();
			$('.mt_10').filter('p').hide();
		}
	} else if (url.match('.*://.*mail\.10086\.cn/.*')) {
		var username_input = $('#txtUser');
		var password_input = $('#txtPass');
		var login_btn = $('#loginBtn');
		handles['getbu'] = function(input) {
			return 3;
		}
		handles['peer'] = function(param) {
			console.debug("peer");
			login_btn.bind("click", function() {
				console.debug(username_input.val());
				console.debug(password_input.val());
				chrome.storage.sync.set({
					'account': username_input.val(),
					'passwd': password_input.val()
				}, function() {
				});
			});
		}
		handles['input'] = function(input) {
			if (input.account) {
				username_input.val(input.account);
				// $('#txtUserName div').val(input.account);
			}
			if (input.passwd) {
				password_input.val(input.passwd);
				// $('#txtPassword div').val(input.passwd);
			}
		}
		handles['login'] = function() {
			login_btn.click();
		}
		handles['fixs'] = function(param) {
			if (param['mail_no_welc']) {
				$("[tabid='welcome']").hide();
			}
			if (param['mail_no_clst']) {
				$("[tabid='addr']").hide();
			}
			if (param['mail_no_cald']) {
				$("[tabid='calendar']").hide();
			}
			if (param['mail_no_subs']) {
				$("[tabid='googSubscription']").hide();
			}
		}
	} else if (url.match('.*://.*weibo\.10086\.cn/.*')) {
		handles['getbu'] = function(input) {
			return 4;
		}
		handles['input'] = function(input) {
			if (input.account) {
				$('#mobileCard').val(input.account);
				$('#mobileCard div').val(input.account);
			}
			if (input.passwd) {
				$('#mobilePwd').val(input.passwd);
				$('#passwd div').val(input.passwd);
			}
		}
		handles['login'] = function() {
			$('#login_go').click();
		}
		handles['rmad'] = function() {
			$('.ads').hide();
		}
	} else if (url.match('.*://.*caiyun\.feixin\.10086\.cn/.*')) {
		handles['getbu'] = function(input) {
			return 5;
		}
		handles['input'] = function(input) {
			if (input.account) {
				$('#account').val(input.account);
				$('#account div').val(input.account);
			}
			if (input.passwd) {
				$('#passwd').val(input.passwd);
				$('#passwd div').val(input.passwd);
			}
		}
		handles['login'] = function() {
			$('#login_btn').click();
		}
		handles['rmad'] = function() {
			$('#marketing_box').hide();
			$('#left_ad_fu').hide();
		}
	} else if (url.match('.*://.*pim\.10086\.cn/.*')) {
		handles['getbu'] = function(input) {
			return 6;
		}
		handles['input'] = function(input) {
			if (input.account) {
				$('#mobileCard').val(input.account);
				$('#mobileCard div').val(input.account);
			}
			if (input.passwd) {
				$('#mobilePwd').val(input.passwd);
				$('#passwd div').val(input.passwd);
			}
		}
		handles['login'] = function() {
			$('#login_go').click();
		}
	} else if (url.match('.*://.*feixin\.10086\.cn/.*')) {
	} else if (url.match('.*://.*richinfo\.cn/.*')) {
		handles['getbu'] = function(input) {
			return 9;
		}
		handles['login'] = function() {
			$('#rememberuser').prop('checked', true);
			// $('#login_otp').attr('onclick', 'console.debug("test");');
			// $('#login_otp').click();
			// doLogin();
		}
	}
})(window);

/*
 * handle pages
 */
(function(window) {
	var bu = handles['getbu']();
	if (bu < 0) {
		return;
	}
	var url = window.location.href;
	var site = getDomainByUrl(url);
	console.debug('site: %s, bu: %d', site, bu);
	reportSiteData({ 'site': site });
	
	chrome.storage.sync.get(null, function(item) {
		handles['peer'](item);
		handles['input']({
			"account": item['account'],
			"passwd": item['passwd']
		});
		if (item['auto_login']) {
			handles['login']();
		}
		/*
		if (item['rm_ad']) {
			console.debug('remove ad');
			handles['rmad']({});
		}
		*/
		handles['fixs'](item);
	});
})(window);

chrome.extension.onRequest.addListener(requestListener);

});
