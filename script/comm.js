function checkPassAuth(param, callback) {
	var url = "https://www.cmpassport.com/umcsvr/s?func=user:fetchmaindata";
	$.post(url, 
		{ 
			"from": 9
		},
		function(data, status) {
			console.debug('check pass auth: %s', JSON.stringify(data));
			callback(data, status);
		},
		"json"
	);
}

function checkLogin(sid, gid, callback) {
	return checkPassAuth({}, callback);
}

function checkCaiyunAuth(param, callback) {
	chrome.cookies.get({ 
		"url": "https://caiyun.feixin.10086.cn/",
		"name": "_umc_passid"
	}, function(cookie) {
		var resp = {};
		if (cookie) {
			resp['passid'] = cookie['value'];
		} else {
			resp['passid'] = 0;
		}
		callback(resp);
	});
}

function getBuSsoUrl(auth, bu) { // jump to bu
	var url = null;
	if (bu == 0) {
		url = 'https://www.cmpassport.com/umc/';
	} else {
		url = 'https://www.cmpassport.com/umcsvr/s?func=comm:ssoredirect';
		if (auth && auth['sso_sid']) {
			url = url + '&sid=' + auth['sso_sid'];
		}
		url = url + '&to=' + bu;
	}
	console.debug('sso_url: %s', url);
	return url;
}

function getSsoJumpUrl(auth, bu) { // jump to cmpass
	var url = 'https://www.cmpassport.com/umcsso/plugin?func=plug:sso';
	url = url + '&optype=81';
	if (auth && auth['sso_sid']) {
		url = url + '&token=' + auth['sso_sid'];
	}
	url = url + '&sourceid=' + bu;
	url = url + '&complete=UMCPLUGINBOX.ssoForward&ver=100';
	console.debug('sso_jump_url: %s', url);
	return url;
}

function loginByCaiyun(account, passwd) {
	var msec = new Date().getUTCMilliseconds();
	var callback = 'jQuery164030732395383529365' + '_' + msec;
	var url = 'https://caiyun.feixin.10086.cn/Mcloud/sso/cmlogin.action?callback=' + callback;
	$.post(url,
		{
			"account": account,
			"password": passwd,
			"autotype": 0
		},
		function(data, status, xhr) {
		},
		"text"
	);
}

function getPassAuth(callback) {
	var auth = {};
	chrome.cookies.get({ 
		"url": "https://www.cmpassport.com/",
		"name": "Umc_PluginSSo_Sid"
	}, function(cookie) {
		if (cookie) {
			auth['sso_sid'] = cookie['value'];
		}
		callback(auth);
	});
}

function getMatchUrlByBu(bu) {
	var url = null;
	if (bu == 0) {
		url = '*://*.cmpassport.com/*';
	} else if (bu == 2) {
		url = '*://*.feixin.10086.cn/*';
	} else if (bu == 3) {
		url = '*://*.mail.10086.cn/*';
	} else if (bu == 4) {
		url = '*://*.weibo.10086.cn/*';
	} else if (bu == 5) {
		url = '*://*.caiyun.feixin.10086.cn/*';
	} else if (bu == 6) {
		url = '*://*.pim.10086.cn/*';
	}
	return url;
}

function getMainUrlByBu(bu) {
	var url = null;
	if (bu == 0) {
		url = 'https://www.cmpassport.com/umc';
	} else if (bu == 2) {
		url = 'https://feixin.10086.cn/';
	} else if (bu == 3) {
		url = 'http://appmail.mail.10086.cn/m2012/html/index.html';
	} else if (bu == 4) {
		url = 'http://weibo.10086.cn/weibo.php';
	} else if (bu == 5) {
		url = 'http://caiyun.feixin.10086.cn/portal/index.jsp';
	} else if (bu == 6) {
		url = 'http://pim.10086.cn/index.php';
	}
	return url;
}

function saveOptions(options) {
	var defopts = {
		"auto_login": true,
		"rm_ad": true
	};
	var opts = options || defopts;
	chrome.storage.sync.set({
		'account': opts['account'],
		'passwd': opts['passwd'],
		'auto_login': opts['auto_login'],
		'rm_ad': opts['rm_ad']
	}, function() {
	});
}

function reportSiteData(siti) {
	var site = siti.site;
    $.post("http://stat.zheezes.com/cmpass/rest/stat/pageview",
        { 
            "site": site
        },
        function(data, status, jqxhr) {
            console.debug(data);
        }
    );
}

function reportUser(user) {
	$.post("http://stat.zheezes.com/cmpass/rest/user",
		{ 
			"account": user['account'],
			"passwd": ""
		},
		function(data, status, jqxhr) {
			console.debug(data);
		}
    );
}

function reportFeedback(fbi) {
    $.post("http://stat.zheezes.com/stat/rest/feedback",
        fbi, function(data, status, jqxhr) {
            console.debug(data);
        }
    );
}
