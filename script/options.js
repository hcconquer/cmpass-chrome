$(function() {

console.log('options start');

var manifest = chrome.runtime.getManifest();

chrome.storage.sync.get(null, function(item) {
	$('#account').val(item['account']);
	$('#passwd').val(item['passwd']);
	$('#auto_login').prop('checked', item['auto_login']);
	$('#rm_ad').prop('checked', item['rm_ad']);
	$('#mail_no_welc').prop('checked', item['mail_no_welc']);
	$('#mail_no_clst').prop('checked', item['mail_no_clst']);
	$('#mail_no_cald').prop('checked', item['mail_no_cald']);
	$('#mail_no_subs').prop('checked', item['mail_no_subs']);
});

$('#saveopt').on('click', function() {
	chrome.storage.sync.set({
		'account': $('#account').val(),
		'passwd': $('#passwd').val(),
		'auto_login': $('#auto_login').prop('checked'),
		'rm_ad': $('#rm_ad').prop('checked'),
		'mail_no_welc': $('#mail_no_welc').prop('checked'),
		'mail_no_clst': $('#mail_no_clst').prop('checked'),
		'mail_no_cald': $('#mail_no_cald').prop('checked'),
		'mail_no_subs': $('#mail_no_subs').prop('checked')
	}, function() {
	});
	reportUser({
		'account': $('#account').val(),
		'passwd': $('#passwd').val(),
	});
});

$('#fbsub').on('click', function() {
	reportFeedback({
		'appname': 'cmpass-chrome',
		'appver': manifest['version'],
		'userid': $('#account').val(),
		'fbtext': $('#fbtext').val()
	});
});

$('title').text(chrome.i18n.getMessage('title'));
$('#version').text(manifest.version);

});

